#!/usr/bin/env zsh

# Fail early, fail often.
set -eu -o pipefail

if [[ "$1" == "default" ]]; then
  # Update apps to use light theme.
  sed -i -e 's/theme = ".*"/theme = "onelight"/' ${HOME}/.config/helix/config.toml
  # sed -i -e "/alacritty\/themes\/themes\//s/gruvbox_dark.yaml/atom_one_light.yaml/g" ${HOME}/.config/alacritty/alacritty.yml
  sed -i -e '/AUTOSUGGEST/s/4e4e4e/b2b2b2/g' ~/.zshrc
  rm ${HOME}/.config/kitty/theme.conf
  ln -s ${HOME}/.config/kitty/kitty-themes/themes/AtomOneLight.conf ${HOME}/.config/kitty/theme.conf
  # sed -i -e "/--theme=/s/\"Intellij Darcula\"/\"OneHalfLight\"/g" ${HOME}/.config/bat/config
else
  # Update apps to use dark theme.
  sed -i -e 's/theme = ".*"/theme = "darcula-solid"/' ${HOME}/.config/helix/config.toml
  # sed -i -e "/alacritty\/themes\/themes\//s/atom_one_light.yaml/gruvbox_dark.yaml/g" ${HOME}/.config/alacritty/alacritty.yml
  sed -i -e '/AUTOSUGGEST/s/b2b2b2/4e4e4e/g' ~/.zshrc
  rm ${HOME}/.config/kitty/theme.conf
  ln -s ${HOME}/.config/kitty/kitty-themes/themes/JetBrains_Darcula.conf ${HOME}/.config/kitty/theme.conf
  # sed -i -e "/--theme=/s/\"OneHalfLight\"/\"Intellij Darcula\"/g" ${HOME}/.config/bat/config
fi
