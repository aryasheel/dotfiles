# Set the prompt
precmd() { print -rP "%~" }
PROMPT='%(?.%F{green}>.%F{red}>)%f '

# Defining some common aliases
alias l='exa --long --all --color=always --color-scale --group-directories-first --icons --header --no-user --time-style=long-iso'
alias lsm='exa --long --all --color=always --color-scale --group-directories-first --icons --sort modified --header --no-user --time-style=long-iso'
alias t='exa --tree --level=5 --all --color=always --color-scale --icons --sort modified --header --no-user --time-style=long-iso'
alias md='mkdir -p'
alias rd='rmdir'
alias zedit='$EDITOR ~/.config/zsh/.zshrc && zgenom reset && source ~/.config/zsh/.zshrc && zgenom clean'
alias alaconf='$EDITOR ~/.config/alacritty/alacritty.yml'
alias kittyconf='$EDITOR ~/.config/kitty/kitty.conf'
alias espconf='$EDITOR ~/.config/espanso/match/base.yml'
alias em='emacsclient -c &'
alias emt='emacsclient -nw'
alias hxlang='$EDITOR ~/.config/helix/languages.toml'
alias keyconf='$EDITOR ~/.config/keymapper/keymapper.conf'
alias logout='loginctl terminate-user aryasheel'
alias mbsync=mbsync -c "$XDG_CONFIG_HOME"/isync/mbsyncrc
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
alias tp='trash-put'
alias te='trash-empty'
alias tl='trash-list'
alias tr='trash-restore'
alias trm='trash-rm'

# Source fzf keybindings
source /usr/share/fzf/shell/key-bindings.zsh

# Change zsh directory when navigating to a new one in lf file manager
LFCD="/home/aryasheel/.config/lf/lfcd.sh"                                #  pre-built binary, make sure to use absolute path
if [ -f "$LFCD" ]; then
    source "$LFCD"
fi

bindkey -s '^o' 'lfcd\n'  # Shortcut for above function

# Function to make a directory and enter it using a single command
mkcd () {
  mkdir -p -- "$1" && cd -- "$1" 
}

# Function to create directories if they don't exist when using the 'mv' command
mvmk () {
  mkdir -p -- "$2"
  mv -- "$1" "$2"
}

# Search inside files using fzf and rga
fif() {
  if [ ! "$#" -gt 0 ]; then echo "Need a string to search for!"; return 1; fi
  rg --files-with-matches --no-messages "$1" | fzf --preview "highlight -O ansi -l {} 2> /dev/null | rg --colors 'match:bg:yellow' --ignore-case --pretty --context 10 '$1' || rg --ignore-case --pretty --context 10 '$1' {}"
}

# Search for flatpak packages using fzf!
# CLR=$(for i in {0..7}; do echo "tput setaf $i"; done)
BLK=\$(tput setaf 0); RED=\$(tput setaf 1); GRN=\$(tput setaf 2); YLW=\$(tput setaf 3); BLU=\$(tput setaf 4); 
MGN=\$(tput setaf 5); CYN=\$(tput setaf 6); WHT=\$(tput setaf 7); BLD=\$(tput bold); RST=\$(tput sgr0);    

AWK_VAR="awk -v BLK=${BLK} -v RED=${RED} -v GRN=${GRN} -v YLW=${YLW} -v BLU=${BLU} -v MGN=${MGN} -v CYN=${CYN} -v WHT=${WHT} -v BLD=${BLD} -v RST=${RST}"

# Searches only from flathub repository
fzf-flatpak-install-widget() {
  flatpak remote-ls flathub --cached --columns=app,name,description \
  | awk -v cyn=$(tput setaf 6) -v blu=$(tput setaf 4) -v bld=$(tput bold) -v res=$(tput sgr0) \
  '{
    app_info=""; 
    for(i=2;i<=NF;i++){
      app_info=cyn app_info" "$i 
    };
    print blu bld $2" -" res app_info "|" $1
    }' \
  | column -t -s "|" -R 3 \
  | fzf \
    --ansi \
    --with-nth=1.. \
    --prompt="Install > " \
    --preview-window "nohidden,40%,<50(down,50%,border-rounded)" \
    --preview "flatpak --system remote-info flathub {-1} | $AWK_VAR -F\":\" '{print YLW BLD \$1 RST WHT \$2}'" \
    --bind "enter:execute(flatpak install flathub {-1})" # when pressed enter it doesn't showing the key pressed but it is reading the input
  zle reset-prompt
}
bindkey '^[f^[i' fzf-flatpak-install-widget #alt-f + alt-i
zle -N fzf-flatpak-install-widget

fzf-flatpak-uninstall-widget() {
  touch /tmp/uns
  flatpak list --columns=application,name \
  | awk -v cyn=$(tput setaf 6) -v blu=$(tput setaf 4) -v bld=$(tput bold) -v res=$(tput sgr0)  \
  '{
    app_id="";
    for(i=2;i<=NF;i++){
      app_id" "$i
    };
    print bld cyn $2 " - " res blu $1
   }' \
  | column -t \
  | fzf \
    --ansi \
    --with-nth=1.. \
    --prompt="  Uninstall > " \
    --header="M-u: Uninstall | M-r: Run" \
    --header-first \
    --preview-window "nohidden,50%,<50(up,50%,border-rounded)" \
    --preview  "flatpak info {3} | $AWK_VAR -F\":\" '{print RED BLD  \$1 RST \$2}'" \
    --bind "alt-r:change-prompt(Run > )+execute-silent(touch /tmp/run && rm -r /tmp/uns)" \
    --bind "alt-u:change-prompt(Uninstall > )+execute-silent(touch /tmp/uns && rm -r /tmp/run)" \
    --bind "enter:execute(
    if [ -f /tmp/uns ]; then 
      flatpak uninstall {3}; 
    elif [ -f /tmp/run ]; then
      flatpak run {3}; 
    fi
    )" # same as the install one but when pressed  entered the message is something like this 
# "Proceed with these changes to the system installation? [Y/n]:" but it will uninstall the selected app weird but idk y
  rm -f /tmp/{uns,run} &> /dev/null
  zle reset-prompt
}
bindkey '^[f^[u' fzf-flatpak-uninstall-widget #alt-f + alt-u
zle -N fzf-flatpak-uninstall-widget

# ripgrep-all can search within all kinds of files and packages
# This function integrates it with fzf
rga-fzf() {
	RG_PREFIX="rga --files-with-matches"
	local file
	file="$(
		FZF_DEFAULT_COMMAND="$RG_PREFIX '$1'" \
			fzf --sort --preview="[[ ! -z {} ]] && rga --pretty --context 5 {q} {}" \
				--phony -q "$1" \
				--bind "change:reload:$RG_PREFIX {q}" \
				--preview-window="70%:wrap"
	)" &&
	echo "opening $file" &&
	xdg-open "$file"
}

scroll-and-clear-screen() {
    printf '\n%.0s' {1..$LINES}
    zle clear-screen
}
zle -N scroll-and-clear-screen
bindkey '^l' scroll-and-clear-screen

# Setting aliases for the above two functions
alias fflat="fzf-flatpak-install-widget"
alias fflatun="fzf-flatpak-uninstall-widget"

# Lines configured by zsh-newuser-install

# Settings related to command history
HISTSIZE=10000
SAVEHIST=10000

# Enable colours
autoload -U colors && colors

# Setting zsh options
# Uses 'cd' as default command if no command is entered
setopt autocd

# Allows for regex matching in search
setopt extendedglob

# If a pattern for filename generation has no matches, print an error
setopt nomatch

# Notifies about background jobs immediately
setopt notify

# Doesn't store duplicate commands in the history
setopt histignoredups

# Doesn't allow overwriting of files, can be overriden
# temporarily using >!
setopt noclobber

# Don't save history for commands/aliases starting
# with spaces
setopt hist_ignore_space

# Tries to correct the spelling of commands
setopt correct

# If a cd command is not passed a directory (also with autocd)
# then try to run as if it were preceeded by ~
setopt cdablevars

# No c-s/c-q output freezing
setopt noflowcontrol

# Allow expansion in prompts
setopt prompt_subst

# This is default, but set for share_history
setopt append_history

# Save each command's beginning timestamp and the duration to the history file
setopt extended_history

# Display PID when suspending processes as well
setopt longlistjobs

# Whenever a command completion is attempted, make sure the entire command path
# is hashed first.
setopt hash_list_all

# Not just at the end
setopt completeinword

# Use zsh style word splitting
setopt noshwordsplit

# Allow use of comments in interactive code
setopt interactivecomments

# Turn off beeing in zsh
unsetopt beep

# Hotkey to enter into vi/emacs mode
bindkey -e

###########
# These are some more options that might warrant being on higher insanity levels,
# but since I don't use them... I'll leave them out for now

# Watch for everyone but me and root
#watch=(notme root)

# Automatically remove duplicates from these arrays
#typeset -U path cdpath fpath manpath

# Import new commands from the history file also in other zsh-session
#setopt share_history

# If a new command line being added to the history list duplicates an older
# one, the older command is removed from the list
setopt histignorealldups

# Remove command lines from the history list when the first character on the
# line is a space
#setopt histignorespace

# Don't send SIGHUP to background processes when the shell exits.
#setopt nohup

# make cd push the old directory onto the directory stack.
#setopt auto_pushd

# Don't push the same dir twice.
setopt pushd_ignore_dups

# * shouldn't match dotfiles. ever.
#setopt noglobdots

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/aryasheel/.config/zsh/.zshrc'
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache

autoload -Uz compinit
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"
#compinit
# End of lines added by compinstall

# load zgenom
source "${HOME}/.config/zgenom/zgenom.zsh"

# Check for plugin and zgenom updates every 7 days
# This does not increase the startup time.
zgenom autoupdate

# if the init script doesn't exist
if ! zgenom saved; then
    echo "Creating a zgenom save"

    # Add this if you experience issues with missing completions or errors mentioning compdef.
    # zgenom compdef

    # Loading extensions
    zgenom load Aloxaf/fzf-tab
    zgenom load zdharma-continuum/fast-syntax-highlighting
    zgenom load zsh-users/zsh-autosuggestions
    zgenom load chrissicool/zsh-256color
    zgenom load Tarrasch/zsh-command-not-found
    zgenom load zpm-zsh/colorize
    zgenom load hcgraf/zsh-sudo
    zgenom load oz/safe-paste
    zgenom load ajeetdsouza/zoxide
    zgenom load le0me55i/zsh-extract
    zgenom load leophys/zsh-plugin-fzf-finder
    zgenom load MichaelAquilina/zsh-you-should-use

    # Provide auto completions that appear as grey text
    zgenom load zsh-users/zsh-completions
    
    # Setting up zoxide for zsh
    eval "$(zoxide init zsh)"

    # Save all to init script
    zgenom save

    # Compile your zsh files
    zgenom compile "$HOME/.config/zsh/.zshrc"
    zgenom compile "$HOME/.config/zgenom"

    # You can perform other "time consuming" maintenance tasks here as well.
    # If you use `zgenom autoupdate` you're making sure it gets
    # executed every 7 days.

    # rbenv rehash
fi
