#!/usr/bin/env zsh

gsettings monitor org.gnome.desktop.interface color-scheme \
  | xargs -L1 bash -c "source ${HOME}/.config/helix/scripts/synchronise-with-system-colour-scheme/update.sh"
