#!/usr/bin/env zsh

# Enables the service to start on boot and also starts it up immediately.
systemctl --user enable helix-system-colour-scheme-synchronisation.service
systemctl --user start helix-system-colour-scheme-synchronisation.service