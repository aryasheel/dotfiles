#!/usr/bin/env zsh

# Stop the service immediately and disables it from starting on boot.
systemctl --user stop helix-system-colour-scheme-synchronisation.service
systemctl --user disable helix-system-colour-scheme-synchronisation.service